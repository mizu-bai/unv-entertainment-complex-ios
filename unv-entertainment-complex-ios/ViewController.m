//
//  ViewController.m
//  unv-entertainment-complex-ios
//
//  Created by mizu bai on 2021/7/21.
//

#import <AudioToolbox/AudioToolbox.h>
#import "ViewController.h"

@interface ViewController ()

@property(nonatomic, assign) SystemSoundID vibrateMode;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Masturbation", nil) message:NSLocalizedString(@"Please use the corner of new version phone to masturbate.", nil) preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"No please", nil) style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Come on", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self modeSelectionSegDidChange:nil];
        [self continueButtonDidClick:nil];
    }];
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)modeSelectionSegDidChange:(UISegmentedControl *)sender {
    [self pauseButtonDidClick:nil];
    switch (sender.selectedSegmentIndex) {
        case 0:
            self.vibrateMode = kSystemSoundID_Vibrate;
            break;
        case 1:
            self.vibrateMode = 1519;
            break;
        case 2:
            self.vibrateMode = 1520;
            break;
        case 3:
            self.vibrateMode = 1521;
            break;
        default:
            break;
    }
    [self continueButtonDidClick:nil];
}

- (IBAction)pauseButtonDidClick:(id)sender {
    AudioServicesDisposeSystemSoundID(self.vibrateMode);
    AudioServicesRemoveSystemSoundCompletion(self.vibrateMode);
}

- (IBAction)continueButtonDidClick:(id)sender {
    AudioServicesAddSystemSoundCompletion(self.vibrateMode, NULL, NULL, systemAudioCallback, NULL);
    AudioServicesPlaySystemSound(self.vibrateMode);
}

#pragma mark - audio callback
void systemAudioCallback(SystemSoundID soundID, void *clientData) {
    AudioServicesPlaySystemSound(soundID);
}


@end
