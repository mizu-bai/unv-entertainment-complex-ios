//
//  SceneDelegate.h
//  unv-entertainment-complex-ios
//
//  Created by mizu bai on 2021/7/21.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

